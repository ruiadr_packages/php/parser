<?php

namespace Ruiadr\Parser;

use Ruiadr\Parser\Base\ParserBase;

class AlternateParser extends ParserBase
{
    final protected function getTag(): string
    {
        return 'link';
    }

    final protected function getAttribute(): string
    {
        return 'href';
    }

    final protected function getAttributesFilters(): array
    {
        return [
            'rel' => 'alternate',
        ];
    }
}
