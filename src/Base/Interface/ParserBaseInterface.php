<?php

namespace Ruiadr\Parser\Base\Interface;

use Ruiadr\Base\Wrapper\Interface\UrlInterface;

interface ParserBaseInterface
{
    /**
     * Construction du parser HTML en utilisant l'objet $url passé en paramètre.
     *
     * @param UrlInterface $url Objet Url utilisé pour la construction du parser
     *
     * @return ParserBaseInterface L'instance du parser
     */
    public static function buildFromUrl(UrlInterface $url): ParserBaseInterface;

    /**
     * Construction du parser HTML en utilisant la chaîne $urlString passée en paramètre.
     *
     * @param string $urlString Chaîne utilisée pour la construction du parser
     *
     * @return ParserBaseInterface L'instance du parser
     */
    public static function buildFromUrlString(string $urlString): ParserBaseInterface;

    /**
     * Retourne une liste des éléments recherchés dans l'URL de la page
     * HTML ciblée.
     *
     * @return array Résultat du parsing
     */
    public function getCollection(): array;
}
