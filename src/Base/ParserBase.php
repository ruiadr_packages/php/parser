<?php

namespace Ruiadr\Parser\Base;

use Ruiadr\Base\Wrapper\Interface\UrlInterface;
use Ruiadr\Base\Wrapper\Url;
use Ruiadr\Fetcher\HtmlFetcher;
use Ruiadr\Parser\Base\Interface\ParserBaseInterface;
use Ruiadr\Utils\StringUtils;

abstract class ParserBase implements ParserBaseInterface
{
    private const VQ = '@@@@'; // Quote virtuelle.

    private ?array $collection = null;

    /**
     * @param string $content Contenu à parser
     */
    public function __construct(private readonly string $content)
    {
    }

    final public static function buildFromUrl(UrlInterface $url): ParserBaseInterface
    {
        return new static((new HtmlFetcher($url))->getContent());
    }

    final public static function buildFromUrlString(string $urlString): ParserBaseInterface
    {
        return self::buildFromUrl(new Url($urlString));
    }

    /**
     * Doit retourner la balise HTML ciblée par le parser.
     * Exemple: "a" pour une balise "<a>".
     *
     * @return string Le tag HTML associé au parser
     */
    abstract protected function getTag(): string;

    /**
     * Doit retourner l'attribut ciblé de la balise HTML.
     * Exemple: "href" pour une balise "<a>".
     *
     * @return string L'attribut associé au parser
     */
    abstract protected function getAttribute(): string;

    /**
     * Si la balise ciblée fait référence à un fichier, possibilité
     * d'appliquer un filtre par extension.
     *
     * Par exemple, si on souhaite récupérer les fichiers JavaScript qui
     * portent l'extension "js", la méthode devra retourner ['js'].
     *
     * Par défaut, le tableau retourné est vide pour récupérer
     * les fichiers de n'importe quelle extension. À surcharger pour
     * personnaliser le résultat attendu.
     *
     * @return array Les extensions associées au parser
     */
    protected function getExtensions(): array
    {
        return [];
    }

    /**
     * Filtrer les balises à récupérer en fonction d'un ou plusieurs
     * attributs et de leurs valeurs.
     *
     * Par exemple, si on souhaite récupérer les balises "links"
     * qui possèdent l'attribut rel="canonical", le tableau retourné
     * devra être de la forme ['rel' => 'canonical'].
     *
     * Clé et valeur doivent être du type "string" pour être pris en
     * compte par la classe.
     *
     * @return array Les attributs qui serviront de filtre au parser
     */
    protected function getAttributesFilters(): array
    {
        return [];
    }

    /**
     * Retourne la liste des extensions ciblées après avoir réalisé un
     * traitement sur les valeurs retournées par la méthode "getExtensions()".
     *
     * @return array Les extensions après nettoyage
     */
    private function getFilteredExtensions(): array
    {
        return array_unique(array_filter(array_map(
            fn (mixed $value): ?string => is_string($value)
                    ? preg_replace('/[^a-z0-9]/', '', StringUtils::lowerTrim($value))
                    : null,
            $this->getExtensions()
        )));
    }

    /**
     * Retourne les patterns d'expressions régulières permettant d'extraire
     * les balises HTML d'un contenu en s'appuyant :
     *      - Sur le tag de la balise HTML
     *      - Son attribut
     *      - Éventuellement : l'extension lorsqu'il s'agit d'un fichier disponible
     *                        dans l'attribut ciblé par la méthode "getAttribute()".
     *
     * @return array Patterns d'expressions régulières permettant de réaliser le parsing
     */
    private function getBasePatterns(): array
    {
        $filteredExtensions = $this->getFilteredExtensions();

        $extsRegex = !empty($filteredExtensions)
            ? '\.'.implode('|', $filteredExtensions).'[^'.self::VQ.']*'
            : '';

        $regex = '/<'.$this->getTag().'.*(?:'.$this->getAttribute().'='.self::VQ.')'
            .'([^'.self::VQ.']*'.$extsRegex.')'.self::VQ.'.*>/m';

        return [
            str_replace(self::VQ, "'", $regex),
            str_replace(self::VQ, '"', $regex),
        ];
    }

    /**
     * Retourne les patterns d'expressions régulières permettant d'extraire
     * les balises HTML d'un contenu, en s'appuyant sur la présence d'un
     * ou plusieurs attributs, et les valeurs qui y sont associées.
     *
     * @return array Patterns d'expressions régulières permettant de réaliser le parsing
     *               sur des attributs particuliers et de leurs valeurs
     */
    private function getAttributesPatterns(): array
    {
        $result = [];
        $filters = $this->getAttributesFilters();

        if (!empty($filters) && !array_is_list($filters)) {
            foreach ($filters as $key => $value) {
                if (is_string($key) && is_string($value)) {
                    $value = preg_replace('/\s+/', '|', trim($value));

                    $regex = '/'.trim($key).'[^=]*=[^'.self::VQ.']*'.self::VQ.'[^'.self::VQ.']*'
                        .'(?:'.$value.')[^'.self::VQ.']*'.self::VQ.'/m';

                    $result[] = str_replace(self::VQ, "'", $regex);
                    $result[] = str_replace(self::VQ, '"', $regex);
                }
            }
        }

        return $result;
    }

    /**
     * Retourne la liste des éléments valides en s'appuyant sur les
     * expressions régulières générées par la méthode "getBasePatterns()".
     * Aucun filtrage sur les attributs n'est réalisé par cette méthode, c'est
     * la méthode "filterData()" qui s'en occupe.
     *
     * @return array Liste des éléments parsés sans filtrage sur les attributs
     */
    private function getData(): array
    {
        $result = [];

        foreach ($this->getBasePatterns() as $pattern) {
            $matches = [];
            preg_match_all($pattern, $this->content, $matches);
            if (count($matches) > 1 && count($matches[0]) > 0) {
                foreach ($matches[0] as $index => $value) {
                    $result[] = [
                        'matches' => $matches[0][$index],
                        'attribute' => $matches[1][$index],
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * Filtre les données $data passées en paramètre, en s'appuyant sur les
     * expressions régulières générées par la méthode "getAttributesPatterns()".
     *
     * @param array $data Les éléments récupérés du parser avant filtrage sur les attributs
     *
     * @return array Les éléments filtrés en fonction des attributs
     */
    private function filterData(array $data): array
    {
        $result = [];
        $attrPatterns = $this->getAttributesPatterns();

        if (empty($attrPatterns)) {
            foreach ($data as $entry) {
                $result[] = $entry['attribute'];
            }
        } else {
            foreach ($data as $entry) {
                foreach ($attrPatterns as $pattern) {
                    $matches = [];
                    preg_match_all($pattern, $entry['matches'], $matches);
                    if (!empty($matches) && count($matches[0]) > 0) {
                        $result[] = $entry['attribute'];
                    }
                }
            }
        }

        return array_unique($result);
    }

    final public function getCollection(): array
    {
        return $this->collection ??= $this->filterData($this->getData());
    }
}
