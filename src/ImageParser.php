<?php

namespace Ruiadr\Parser;

use Ruiadr\Parser\Base\ParserBase;

class ImageParser extends ParserBase
{
    final protected function getTag(): string
    {
        return 'img';
    }

    final protected function getAttribute(): string
    {
        return 'src';
    }
}
