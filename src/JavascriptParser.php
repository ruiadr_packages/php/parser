<?php

namespace Ruiadr\Parser;

use Ruiadr\Parser\Base\ParserBase;

class JavascriptParser extends ParserBase
{
    final protected function getTag(): string
    {
        return 'script';
    }

    final protected function getAttribute(): string
    {
        return 'src';
    }

    final protected function getExtensions(): array
    {
        return ['js'];
    }
}
