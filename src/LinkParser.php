<?php

namespace Ruiadr\Parser;

use Ruiadr\Parser\Base\ParserBase;

class LinkParser extends ParserBase
{
    final protected function getTag(): string
    {
        return 'a';
    }

    final protected function getAttribute(): string
    {
        return 'href';
    }
}
