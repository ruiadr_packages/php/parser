<?php

namespace Ruiadr\Parser;

use Ruiadr\Parser\Base\ParserBase;

class StylesheetsParser extends ParserBase
{
    final protected function getTag(): string
    {
        return 'link';
    }

    final protected function getAttribute(): string
    {
        return 'href';
    }

    final protected function getExtensions(): array
    {
        return ['css'];
    }
}
