<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Parser\AlternateParser;

final class AlternateParserTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testAlternateFromURL(): void
    {
        $parser = AlternateParser::buildFromUrlString(self::TEST_DOMAIN);
        $collection = $parser->getCollection();

        $this->assertIsArray($collection);
        $this->assertTrue(count($collection) > 0);
    }
}
