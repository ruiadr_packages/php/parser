<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Parser\CanonicalParser;

final class CanonicalParserTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testCanonicalFromURL(): void
    {
        $parser = CanonicalParser::buildFromUrlString(self::TEST_DOMAIN);
        $collection = $parser->getCollection();

        $this->assertIsArray($collection);
        $this->assertTrue(count($collection) > 0);
    }
}
