<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Parser\ImageParser;

final class ImageParserTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testImageFromURL(): void
    {
        $parser = ImageParser::buildFromUrlString(self::TEST_DOMAIN);
        $collection = $parser->getCollection();

        $this->assertIsArray($collection);
        $this->assertTrue(count($collection) > 0);
    }
}
