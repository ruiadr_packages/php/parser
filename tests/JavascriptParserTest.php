<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Parser\JavascriptParser;

final class JavascriptParserTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testJavascriptFromURL(): void
    {
        $parser = JavascriptParser::buildFromUrlString(self::TEST_DOMAIN);
        $collection = $parser->getCollection();

        $this->assertIsArray($collection);
        $this->assertTrue(count($collection) > 0);
    }
}
