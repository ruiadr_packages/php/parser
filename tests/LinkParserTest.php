<?php

namespace Ruiadr\Fetcher\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Parser\LinkParser;

final class LinkParserTest extends TestCase
{
    final public const TEST_DOMAIN = 'https://phpunit.adrien-ruiz.fr';

    public function testLinksFromURL(): void
    {
        $parser = LinkParser::buildFromUrlString(self::TEST_DOMAIN);
        $collection = $parser->getCollection();

        $this->assertIsArray($collection);
        $this->assertTrue(count($collection) > 0);
    }
}
